var recipe = {
  transcriptionHook: {
    "DISABLE_ALL_MODULES": {
      pattern: "disable all",
      command: "DISABLEMODULES"
    },
    "DISABLE_WEATHER": {
      pattern: "disable weather",
      command: "DISABLEWEATHER"
    },
    "ENABLE_WEATHER": {
      pattern: "enable weather",
      command: "ENABLEWEATHER"
    },
    "ENABLE_CLOCK": {
      pattern: "enable clock",
      command: "ENABLECLOCK"
    },
    "DISABLE_CLOCK": {
      pattern: "disable clock",
      command: "DISABLECLOCK"
    },
    "ENABLE_CALENDAR": {
      pattern: "enable calendar",
      command: "ENABLECALENDAR"
    },
    "ENABLE_ALL_MODULES": {
      pattern: "enable all",
      command: "ENABLEMODULES"
    },

    "DISABLE_CALENDAR": {
      pattern: "disable calendar",
      command: "DISABLECALENDAR"
    },
    "DISABLE_NEWS": {
      pattern: "disable news",
      command: "DISABLENEWS"
    },
    "ENABLE_NEWS": {
      pattern: "enable news",
      command: "ENABLENEWS"
    },
    "HIDE_CAMERA": {
      pattern: "hide camera",
      command: "HIDECAMERA"
    },

    //Notification Part for Recognition

    "RECOGNIZE_ME": {
      pattern: "recognize me",
      command: "RECOGNIZEME"
    },
  },
  command: {
    "DISABLEMODULES": {
      moduleExec: {
        module:()=>{
          return ["calendar","clock", "currentweather","weather","newsfeed"]
        },
        exec: (module, params, key) => {
          module.hide(1000, null, {lockString:"AMK2"})
        }
      }
    },
    "ENABLEMODULES": {
      moduleExec: {
        module:()=>{
          return ["calendar","clock", "currentweather","weather","newsfeed"]
        },
        exec: (module, params, key) => {
          module.show(1000, null, {lockString:"AMK2"})
        }
      }
    },

    //Notification Part for Recognition

    "RECOGNIZEME": {
      notificationExec: {
        notification: (params, key) => {
          // params: result of transcriptionHook match or gAction parameters
          // key: which transcriptionHook or gAction call this command
  
          if (key == "com.example.commands.RECOGNIZEME")
          return "Turn_Recognition_On" //return value will be used as notification name
          //We can use this return value in our module as notification Recieved and perform action based on it
        },
        payload: (params, key)=> {
          return { // return value will be used as payload
            message: "Turn Recognition on",
            timer: 5000,
          }
        }
   
    },
    
    "DISABLECALENDAR":{

        moduleExec: {
          module:(params, key)=>{
            return ["calendar"] // Array of String which contains module names should be returned.
          },
          exec: (module, params, key) => { // `module` will be target module instance.
            module.hide()
          }
        }
      },
      "DISABLEWEATHER":{

        moduleExec: {
          module:(params, key)=>{
            return ["currentweather"] // Array of String which contains module names should be returned.
          },
          exec: (module, params, key) => { // `module` will be target module instance.
            module.hide()
          }
        }
      },
      "ENABLENEWS":{

        moduleExec: {
          module:(params, key)=>{
            return ["newsfeed"] // Array of String which contains module names should be returned.
          },
          exec: (module, params, key) => { // `module` will be target module instance.
            module.show()
          }
        }
      },
      "ENABLECLOCK":{

        moduleExec: {
          module:(params, key)=>{
            return ["clock"] // Array of String which contains module names should be returned.
          },
          exec: (module, params, key) => { // `module` will be target module instance.
            module.show()
          }
        }
      },
      "ENABLECALENDAR":{

        moduleExec: {
          module:(params, key)=>{
            return ["calendar"] // Array of String which contains module names should be returned.
          },
          exec: (module, params, key) => { // `module` will be target module instance.
            module.show()
          }
        }
      },
      "ENABLEWEATHER":{

        moduleExec: {
          module:(params, key)=>{
            return ["currentweather"] // Array of String which contains module names should be returned.
          },
          exec: (module, params, key) => { // `module` will be target module instance.
            module.show()
          }
        }
      },
      "DISABLENEWS":{

        moduleExec: {
          module:(params, key)=>{
            return ["newsfeed"] // Array of String which contains module names should be returned.
          },
          exec: (module, params, key) => { // `module` will be target module instance.
            module.hide()
          }
        }
      },
      "DISABLECLOCK":{

        moduleExec: {
          module:(params, key)=>{
            return ["clock"] // Array of String which contains module names should be returned.
          },
          exec: (module, params, key) => { // `module` will be target module instance.
            module.hide()
          }
        }
      },
      "HIDECAMERA":{

        moduleExec: {
          module:(params, key)=>{
            return ["camera"] // Array of String which contains module names should be returned.
          },
          exec: (module, params, key) => { // `module` will be target module instance.
            module.hide()
          }
        }
      },
    
    


  },
}
}
exports.recipe = recipe
