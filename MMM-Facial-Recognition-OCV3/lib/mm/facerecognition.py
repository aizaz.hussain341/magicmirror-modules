
#!/usr/bin/python
# coding: utf8
"""MMM-Facial-Recognition - MagicMirror Module
Face Recognition Script
The MIT License (MIT)

Copyright (c) 2016 Paul-Vincent Roll (MIT License)
Based on work by Tony DiCola (Copyright 2013) (MIT License)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""
# #Facial Training imports
# from .lib.tools.capture import ToolsCapture
# from .lib.tools.config import ToolsConfig
# from .lib.tools.train import ToolsTrain

# from config import MMConfig

from builtins import input
import time

#Facial Training imports

import numpy as numpy
import argparse
import pyzbar.pyzbar as pyzbar
import signal
from config import MMConfig
import cv2
from face import FaceDetection
import time
import sys
import os
from gtts import gTTS
import subprocess 
import random


language = 'en'
sys.path.append(
    (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/common/'))


MMConfig.toNode("status", "Facerecognition module started...")
# Setup variables
current_user = None
last_match = None
detection_active = True
login_timestamp = time.time()
same_user_detected_in_row = 0

# Load training data into model
MMConfig.toNode("status", 'Loading training data...')

# load the model
model = cv2.face.LBPHFaceRecognizer_create(threshold=MMConfig.getThreshold())

# Load training file specified in config.js
model.read(MMConfig.getTrainingFile())
MMConfig.toNode("status", 'Training data loaded!')

# get camera
camera = MMConfig.getCamera()

def shutdown(self, signum):
    MMConfig.toNode("status", 'Shutdown: Cleaning up camera...')
    camera.stop()
    quit()


signal.signal(signal.SIGINT, shutdown)

# sleep for a second to let the camera warm up
time.sleep(1)

face = MMConfig.getFaceDetection()

# Main Loop
while True:
    # Sleep for x seconds specified in module config
    time.sleep(MMConfig.getInterval())
    # if detecion is true, will be used to disable detection if you use a PIR sensor and no motion is detected
    if detection_active is True:
        # Get image
        color_image = camera.read()
        # Convert image to grayscale.
        image = cv2.cvtColor(color_image, cv2.COLOR_RGB2GRAY)
        # Get coordinates of single face in captured image.
        data_file = open('/home/aizaz/MagicMirror/Update/switcher.txt','r')
        switcher = data_file.readline()
        # switcher = switcher.split()
        # switcher = str(switcher)
        switcher = int(switcher)
        MMConfig.toNode("status", switcher)
    
        if switcher:
            result = None 
            MMConfig.toNode("status", 'Switcher file is if')

        else:
            result = face.detect_single(image)
            MMConfig.toNode("status", 'Switcher file is else ')

        MMConfig.toNode("status", 'Switcher file readed')
        
        # No face found, logout user?
        if switcher:
            decodedObjects = None
            MMConfig.toNode("status", 'QRCode Started!')
            decodedObjects = pyzbar.decode(image)
            if decodedObjects is not None:
                for obj in decodedObjects:
                    if obj.data is not None:
                        wifi_cred = str(obj.data)
                        # if wifi_cred[:4] != "ssid":
                        MMConfig.toNode("status", 'QRCode Data Found!')
                        output_file = open('/home/aizaz/MagicMirror/Update/QRCodeValue.txt', 'w')
                        output_file.write(obj.data)
                        output_file.close()
                        file_data = open('/home/aizaz/MagicMirror/Update/QRCodeValue.txt', 'r')
                        MMConfig.toNode("status", file_data.read())
                        string = file_data.read()
                            # MMConfig.toNode("status", "kiya masla hai yar")
                        input_file = open('/home/aizaz/MagicMirror/modules/Recognition/facial_training.py', 'r')
                        lines= input_file.readlines()
                        output_file = open('/home/aizaz/MagicMirror/modules/Recognition/facial_training.py', 'w')
                        qrdata = open('/home/aizaz/MagicMirror/Update/QRCodeValue.txt', 'r')
                        a=0
                        for line in lines:
                            if a:
                                output_file.write("name=\"" + qrdata.read() + "\" \n")
                                a=0
                            elif line.strip("\n") == "# @namechange":
                                output_file.write(line)
                                a=1
                                continue
                            else:
                                output_file.write(line)

                        config_training_file= open('/home/aizaz/MagicMirror/Update/config_training.js','r')
                        config_data=config_training_file.read()
                        config_training_file.close()
                        
                        config_file=open('/home/aizaz/MagicMirror/config/config.js','w')
                        config_file.write(config_data)
                        config_file.close()

                        os.system("pm2 restart mm")
                        
                    else:
                        MMConfig.toNode("status", "Doing wifi work")
                       
                        # recognition_file=open("/home/mudasir_habib/MagicMirror/modules/MMM-Facial-Recognition-OCV3/MMM-Facial-Recognition-OCV3.js",'r')
                        # lines=recognition_file.readlines()
                        # recognition_file=open("/home/mudasir_habib/MagicMirror/modules/MMM-Facial-Recognition-OCV3/MMM-Facial-Recognition-OCV3.js",'w')
                        # a=0
                        # for line in lines:
                        #     if a:
                        #         recognition_file.write("		var qrval = \"" + file_data.read() + "\"; \n" )
                        #         a=0
                        #     elif line.strip("\n") ==  "// #@qrval":
                        #         recognition_file.write(line)
                        #         a=1
                        #     else:
                        #         recognition_file.write(line)
                    # sys.exit(0)q  `   `
                    # os.system("/home/mudasir_habib/MagicMirror/npm start")    
                    
                    # childpid = os.fork()
                    # os.kill(childpid, signal.SIGINT)
                    # os.system("npm start")

        elif(not switcher):
            if result is None:
                MMConfig.toNode("status", 'Status logout bahar')
                # if last detection exceeds timeout and there is someone logged in -> logout!
                if (current_user is not None and time.time() - login_timestamp > MMConfig.getLogoutDelay()):
                    # callback logout to node helper
                    MMConfig.toNode("status", 'Status logout')
                    MMConfig.toNode("logout", {"user": current_user})
                    same_user_detected_in_row = 0
                    current_user = None
                continue
            MMConfig.toNode("status", 'Recognition Started')
            # result = face.detect_single(image)
            # Set x,y coordinates, height and width from face detection result
            x, y, w, h = result
            # Crop image on face. If algorithm is not LBPH also resize because in all other algorithms image resolution has to be the same as training image resolution.
            crop = face.crop(image, x, y, w, h,int(MMConfig.getFaceFactor() * w))
            # Test face against model.
            label, confidence = model.predict(crop)
            # We have a match if the label is not "-1" which equals unknown because of exceeded threshold and is not "0" which are negtive training images (see training folder).
            if (label != -1 and label != 0):
                # Set login time
                login_timestamp = time.time()
                # Routine to count how many times the same user is detected
                if (label == last_match and same_user_detected_in_row < 2):
                    # if same user as last time increment same_user_detected_in_row +1
                    same_user_detected_in_row += 1
                if label != last_match:
                    # if the user is diffrent reset same_user_detected_in_row back to 0
                    same_user_detected_in_row = 0
                # A user only gets logged in if he is predicted twice in a row minimizing prediction errors.
                if (label != current_user and same_user_detected_in_row > 0):
                    current_user = label
                    # Callback current user to node helper
                    MMConfig.toNode("login", {"user": label, "confidence": str(confidence)})
                    users = open('/home/aizaz/MagicMirror/Update/users.txt', 'r')
                    users=str(users.read()).split(',')
                    messages=["How you are feeling", "Good Morning", "Hello dear", "How was your sleep"]
                    msg_index = random.randint(0, len(messages)-1)
                    myobj = gTTS(text=messages[msg_index] + users[label-1] , lang=language, slow=False)
                    myobj.save("welcome.mp3")
                    subprocess.call(['cvlc', 'welcome.mp3', '--play-and-exit' ])
                        
                # set last_match to current prediction
                last_match = label
            # if label is -1 or 0, current_user is not already set to unknown and last prediction match was at least 5 seconds ago
            # (to prevent unknown detection of a known user if he moves for example and can't be detected correctly)
            elif (current_user != 0 and time.time() - login_timestamp > 5):
                # Set login time
                login_timestamp = time.time()
                # set current_user to unknown
                current_user = 0
                # callback to node helper
                MMConfig.toNode("login", {"user": current_user, "confidence": None})
            else:
                continue
            MMConfig.toNode("status", 'Continue Started')


            
